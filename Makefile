all: parcel
	sudo go run ./main.go | tee logfile

parcel:
	parcel build static/*.html
clean:
	rm -rf dist node_modules package*.json .cache
